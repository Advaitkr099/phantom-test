const express = require('express');
const req = require('express/lib/request');
const app = express();
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const authRoute = require('./routes/auth') 
const postRoute = require('./routes/post')
dotenv.config()
console.log("hello")
mongoose.connect(process.env.DB_CONNECT,{useNewUrlParser:true},()=>{
    console.log("mongodb")
})

app.use(express.json())

app.use('/api/user',authRoute);
app.use('/api/posts',postRoute);

app.listen(3000,()=>console.log(`server Up`))